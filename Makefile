CCFLAGS = -Werror -Wall -Wextra

ifeq ($(OS), Windows_NT)
	CC = C:\msys64\ucrt64\bin\g++.exe
	CCSOURCE = src\main.exe
	DEL = del .\src\main.exe
else
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S), Linux)
		CC = g++
		CCSOURCE = src/main
		DEL = rm -f src/main
	endif
endif

all: clean build

build:
	  $(CC) src/*.cpp -o $(CCSOURCE) $(CCFLAGS)

hexdump: build
	$(CCSOURCE) -c "./test/test02.txt"

dehex: build
	$(CCSOURCE) -d "./test/test01.txt"

clean:
	$(DEL)

.PHONY: all clean build hexdump dehex
