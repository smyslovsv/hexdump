#ifndef HEXDUMP_H
#define HEXDUMP_H

#include <string>
#include <vector>

/* hexdump принимает на входе vector<char> и возвращает шестнадцатеричный дамп
 * (в виде строки std::string) */
bool run_hexdump(std::vector<char> *hexdump, std::string *dehex);

/* dehex принимает на вход строку (std::string) в том же формате, что и вывод,
 * описанный выше для hexdump, и возвращает vector<char> */
bool run_dehex(std::string *dehex, std::vector<char> *hexdump);

#endif