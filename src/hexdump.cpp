#include "hexdump.h"

#include <iostream>
#include <sstream>
#include <string>

bool run_hexdump(std::vector<char> *hexdump, std::string *dehex) {
  bool error = true;

  std::stringstream number_str;
  std::stringstream hex_str;
  std::stringstream text_str;

  int cur_row = 0;
  int cur_col = 0;

  for (char c : *hexdump) {
    if (!cur_col) {
      number_str.width(8);
      number_str.fill('0');
      number_str << std::hex << cur_row;
      number_str << ": ";
    }
    int ch = c;
    hex_str.width(2);
    hex_str.fill('0');

    if (ch >= 32 && ch <= 126) {
      hex_str << std::hex << ch << " ";
      text_str << c;

    } else if (ch >= 0 && ch <= 31) {
      hex_str << std::hex << ch << " ";
      text_str << ".";

    } else
      error = false;

    if (cur_col == 15) {
      dehex->append(number_str.str());
      dehex->append(hex_str.str());
      dehex->append(" ");
      dehex->append(text_str.str());

      number_str.str(std::string());
      hex_str.str(std::string());
      text_str.str(std::string());

      cur_row += 16;
    }

    cur_col = (cur_col + 1) % 16;
  }

  if (number_str.rdbuf()->in_avail()) {
    div_t empty_char = div(hexdump->size(), 16);
    for (int i = empty_char.rem; i < 16; i++) hex_str << ("   ");

    dehex->append(number_str.str());
    dehex->append(hex_str.str());
    dehex->append(" ");
    dehex->append(text_str.str());
  }

  return error;
}

bool run_dehex(std::string *dehex, std::vector<char> *hexdump) {
  bool error = true;

  div_t len = div(dehex->size(), 75);

  int cur_chank = 0;

  while (cur_chank < len.quot) {
    std::string tmp;
    try {
      tmp = dehex->substr(cur_chank * 75, 75);
      tmp = tmp.substr(10, 48);

      while (!tmp.empty()) {
        char ch = std::stoi(tmp.substr(0, 3), 0, 16);
        tmp = tmp.substr(3);
        hexdump->push_back(ch);
      }

      cur_chank++;

    } catch (std::out_of_range &exception) {
      std::cerr << "Wrong input hex format of 'dehex' in function 'run_dehex': "
                << exception.what() << std::endl;
      error = false;
    } catch (std::invalid_argument &exception) {
      std::cerr << "Wrong input hex format of 'dehex' in function 'run_dehex': "
                << exception.what() << std::endl;
      error = false;
    }
  }  // end while

  if (len.rem && !error) {
    try {
      std::string tmp = dehex->substr(cur_chank * 76);

      tmp = tmp.substr(10, 48);

      while (!tmp.empty() && tmp.substr(0, 3) != "   ") {
        char ch = std::stoi(tmp.substr(0, 3), 0, 16);
        tmp = tmp.substr(3);
        hexdump->push_back(ch);
      }
    } catch (std::out_of_range &exception) {
      std::cerr << "Wrong input hex format of 'dehex' in function 'run_dehex': "
                << exception.what() << std::endl;
      error = false;
    } catch (std::invalid_argument &exception) {
      std::cerr << "Wrong input hex format of 'dehex' in function 'run_dehex': "
                << exception.what() << std::endl;
      error = false;
    }
  }

  return error;
}
