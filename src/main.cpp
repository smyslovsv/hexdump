#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "hexdump.h"

#define HEXDUMP 1
#define DEHEX 0

// Выводит справку о программе
void show_help();

// получает тип операции и имя файла из аргументов командной строки
bool get_options(int *argc, char *argv[], int *type, std::string *filename);

// запускает обработчик команды
bool run_handler(int type, std::string filename, std::vector<char> *hexdump,
                 std::string *dehex);

int main(int argc, char *argv[]) {
  int error;

  int type = HEXDUMP;
  std::string filename;
  std::vector<char> hexdump;
  std::string dehex;

  if (get_options(&argc, argv, &type, &filename)) {
    error = run_handler(type, filename, &hexdump, &dehex);
  }

  return error ? 0 : 1;
}

bool run_handler(int type, std::string filename, std::vector<char> *hexdump,
                 std::string *dehex) {
  bool error = true;

  // открываем файл для чтения
  std::ifstream in(filename);
  if (!in) {
    std::cout << "Can't open file '" << filename << "'\n";
    return false;
  }

  char ch;
  if (type) {
    while (in) {
      in.get(ch);
      hexdump->push_back(ch);
    }
  } else {
    while (in) {
      in.get(ch);
      dehex->push_back(ch);
    }
  }

  in.close();

  if (type) {
    error = run_hexdump(hexdump, dehex);
    if (error) std::cout << *dehex << std::endl;

  } else {
    error = run_dehex(dehex, hexdump);
    if (error) {
      for (size_t i = 0; i < hexdump->size(); i++) std::cout << hexdump->at(i);
      std::cout << std::endl;
    }
  }

  return error;
}

bool get_options(int *argc, char *argv[], int *type, std::string *filename) {
  bool error = false;

  if (*argc < 3) {
    show_help();
    exit(EXIT_FAILURE);
  }

  std::string type_operation = argv[1];

  if (type_operation == "-c" || type_operation == "--hexdump") {
    *type = HEXDUMP;
    *filename = argv[2];
    error = true;

  } else if (type_operation == "-d" || type_operation == "--dehex") {
    *type = DEHEX;
    *filename = argv[2];
    error = true;

  } else if (type_operation == "-h" || type_operation == "--help") {
    show_help();
  }

  return error;
}

void show_help() {
  printf("Usage: [mode] FILE\n");
  printf(
      "\n-c, --hexdump  equivalent to -vET\n"
      "-d, --dehex     number nonempty output lines, overrides -n\n"
      "-h, --help      equivalent to -vE\n");
  printf(
      "Example: -c ~/test01.txt - for view file in hex-mode\n -d ./test02.txt "
      "- for view source data from hexdump\n");
}